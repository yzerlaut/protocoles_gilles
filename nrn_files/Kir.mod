UNITS {
	(pA) = (picoamp)
	(nS) = (nanosiemens)
	(mV) = (millivolt)
}

NEURON {
	POINT_PROCESS Kir
	RANGE i
	RANGE tau_m, gmax, E_rev, SLOPE
	ELECTRODE_CURRENT i
	GLOBAL minf
}


PARAMETER {
	tau_m = 0.2 (ms)
	gmax	= 20 (nS)
	E_rev	= -90	(mV)
	SLOPE = -0.074
}

STATE {
        m
}

ASSIGNED { 
    v (mV)
    i (pA) 
    minf
}

BREAKPOINT {
    SOLVE states METHOD cnexp
    i = -gmax*(v-E_rev)*m
}

INITIAL {
	i = 0
	rates(v)
	m = minf
}

DERIVATIVE states { 
        rates(v)
	m' = (minf - m) / tau_m
}

PROCEDURE rates(v(mV)) {
UNITSOFF
        minf = 1/(1+exp(SLOPE*(E_rev-v)))
UNITSON
}